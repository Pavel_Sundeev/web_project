package com.tabvn.DAO;

import com.tabvn.model.АвторыEntity;
import com.tabvn.model.ПользователиEntity;

import java.sql.SQLException;
import java.util.List;

public interface ПользователиDAO {
    //create
    void add(ПользователиEntity user) throws SQLException;

    //read
    List<ПользователиEntity> getAll() throws SQLException;

    //search
     ПользователиEntity getById(int id) throws SQLException;

    //update
    void update(ПользователиEntity user) throws SQLException;

    //delete
    void delete(ПользователиEntity user) throws SQLException;
}
