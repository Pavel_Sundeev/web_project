package com.tabvn.DAO;

import com.tabvn.model.АвторыEntity;

import java.sql.SQLException;
import java.util.List;

public interface АвторыDAO {

    //create
    void add(АвторыEntity author) throws SQLException;

    //read
    List<АвторыEntity> getAll() throws SQLException;

    //search
    АвторыEntity getById(int id) throws SQLException;

    //update
    void update(АвторыEntity author) throws SQLException;

    //delete
    void delete(АвторыEntity author) throws SQLException;
}
