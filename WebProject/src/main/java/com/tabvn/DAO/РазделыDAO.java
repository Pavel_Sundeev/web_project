package com.tabvn.DAO;

import com.tabvn.model.АвторыEntity;
import com.tabvn.model.РазделыEntity;

import java.sql.SQLException;
import java.util.List;

public interface РазделыDAO {
    //create
    void add(РазделыEntity sections) throws SQLException;

    //read
    List<РазделыEntity> getAll() throws SQLException;

    //search
    РазделыEntity getById(int id) throws SQLException;

    //update
    void update(РазделыEntity sections) throws SQLException;

    //delete
    void delete(РазделыEntity sections) throws SQLException;
}
