package com.tabvn.DAO;

import com.tabvn.model.АвторыEntity;
import com.tabvn.model.КатегорииEntity;

import java.sql.SQLException;
import java.util.List;

public interface КатегорииDAO {
    //create
    void add(КатегорииEntity category) throws SQLException;

    //read
    List<КатегорииEntity> getAll() throws SQLException;

    //search
    КатегорииEntity getById(int id) throws SQLException;

    //update
    void update(КатегорииEntity category) throws SQLException;

    //delete
    void delete(КатегорииEntity category) throws SQLException;
}
