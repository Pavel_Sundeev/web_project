package com.tabvn.DAO;

import com.tabvn.model.АвторыEntity;
import com.tabvn.model.ЗаданияEntity;

import java.sql.SQLException;
import java.util.List;

public interface ЗаданияDAO {
    //create
    void add(ЗаданияEntity task) throws SQLException;

    //read
    List<ЗаданияEntity> getAll() throws SQLException;

    //search
    ЗаданияEntity getById(int id) throws SQLException;

    //update
    void update(ЗаданияEntity task) throws SQLException;

    //delete
    void delete(ЗаданияEntity task) throws SQLException;
}
