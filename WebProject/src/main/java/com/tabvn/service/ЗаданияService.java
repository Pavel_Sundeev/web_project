package com.tabvn.service;

import com.tabvn.DAO.ЗаданияDAO;
import com.tabvn.SessionUtil;
import com.tabvn.model.ЗаданияEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.sql.SQLException;
import java.util.List;

public class ЗаданияService extends SessionUtil implements ЗаданияDAO {
    @Override
    public void add(ЗаданияEntity task) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.save(task);
        closeTransaction();
    }

    @Override
    public List<ЗаданияEntity> getAll() throws SQLException {
        openTransaction();
        Session session = getSession();
        String sql = "select * from задания";
        Query query = session.createNativeQuery(sql).addEntity(ЗаданияEntity.class);
        List<ЗаданияEntity> task = query.list();
        closeTransaction();
        return task;
    }

    @Override
    public ЗаданияEntity getById(int id) throws SQLException {
        openTransaction();
        String sql = "select * from задания where id = :id";
        Session session = getSession();
        Query query = session.createNativeQuery(sql).addEntity(ЗаданияEntity.class);
        query.setParameter("id", id);
        ЗаданияEntity task = (ЗаданияEntity) query.getSingleResult();
        closeTransaction();
        return task;
    }

    @Override
    public void update(ЗаданияEntity task) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.update(task);
        closeTransaction();
    }

    @Override
    public void delete(ЗаданияEntity task) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.remove(task);
        closeTransaction();
    }
}
