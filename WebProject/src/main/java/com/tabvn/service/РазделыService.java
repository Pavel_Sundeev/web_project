package com.tabvn.service;

import com.tabvn.DAO.РазделыDAO;
import com.tabvn.SessionUtil;
import com.tabvn.model.РазделыEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.sql.SQLException;
import java.util.List;

public class РазделыService extends SessionUtil implements РазделыDAO {
    @Override
    public void add(РазделыEntity sections) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.save(sections);
        closeTransaction();
    }

    @Override
    public List<РазделыEntity> getAll() throws SQLException {
        openTransaction();
        Session session = getSession();
        String sql = "select * from разделы";
        Query query = session.createNativeQuery(sql).addEntity(РазделыEntity.class);
        List<РазделыEntity> sections = query.list();
        closeTransaction();
        return  sections;
    }

    @Override
    public РазделыEntity getById(int id) throws SQLException {
        openTransaction();
        Session session = getSession();
        String sql = "select * from разделы where id = :id";
        Query query = session.createNativeQuery(sql).addEntity(РазделыEntity.class);
        query.setParameter("id", id);
        РазделыEntity sections = (РазделыEntity) query.getSingleResult();
        closeTransaction();
        return sections;
    }

    @Override
    public void update(РазделыEntity sections) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.update(sections);
        closeTransaction();
    }

    @Override
    public void delete(РазделыEntity sections) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.remove(sections);
        closeTransaction();
    }
}
