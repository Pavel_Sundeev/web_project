package com.tabvn.service;

import com.tabvn.DAO.КатегорииDAO;
import com.tabvn.SessionUtil;
import com.tabvn.model.ЗаданияEntity;
import com.tabvn.model.КатегорииEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.sql.SQLException;
import java.util.List;

public class КатегорииService extends SessionUtil implements КатегорииDAO {
    @Override
    public void add(КатегорииEntity category) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.save(category);
        closeTransaction();
    }

    @Override
    public List<КатегорииEntity> getAll() throws SQLException {
        openTransaction();
        Session session = getSession();
        String sql = "select * from задания";
        Query query = session.createNativeQuery(sql).addEntity(ЗаданияEntity.class);
        List<КатегорииEntity> category = query.list();
        closeTransaction();
        return category;
    }

    @Override
    public КатегорииEntity getById(int id) throws SQLException {
        openTransaction();
        String sql = "select * from задания where id = :id";
        Session session = getSession();
        Query query = session.createNativeQuery(sql).addEntity(ЗаданияEntity.class);
        query.setParameter("id", id);
        КатегорииEntity category = (КатегорииEntity) query.getSingleResult();
        closeTransaction();
        return category;
    }

    @Override
    public void update(КатегорииEntity category) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.update(category);
        closeTransaction();
    }

    @Override
    public void delete(КатегорииEntity category) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.remove(category);
        closeTransaction();
    }
}
