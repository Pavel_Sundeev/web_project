package com.tabvn.service;

import com.tabvn.DAO.ПользователиDAO;
import com.tabvn.SessionUtil;
import com.tabvn.model.КатегорииEntity;
import com.tabvn.model.ПользователиEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.sql.SQLException;
import java.util.List;

public class ПользователиService extends SessionUtil implements ПользователиDAO {
    @Override
    public void add(ПользователиEntity user) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.save(user);
        closeTransaction();
    }

    @Override
    public List<ПользователиEntity> getAll() throws SQLException {
        openTransaction();
        Session session = getSession();
        String sql = "select * from пользователи";
        Query query = session.createNativeQuery(sql).addEntity(ПользователиEntity.class);
        List<ПользователиEntity> user = query.list();
        closeTransaction();
        return user;
    }

    @Override
    public ПользователиEntity getById(int id) throws SQLException {
        openTransaction();
        Session session = getSession();
        String sql = "select * from пользователи where id = :id";
        Query query = session.createNativeQuery(sql).addEntity(ПользователиEntity.class);
        query.setParameter("id", id);
        ПользователиEntity user = (ПользователиEntity) query.getSingleResult();
        closeTransaction();
        return user;
    }

    @Override
    public void update(ПользователиEntity user) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.update(user);
        closeTransaction();
    }

    @Override
    public void delete(ПользователиEntity user) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.remove(user);
        closeTransaction();
    }
}
