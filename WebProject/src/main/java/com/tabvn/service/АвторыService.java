package com.tabvn.service;

import com.tabvn.DAO.АвторыDAO;
import com.tabvn.SessionUtil;
import com.tabvn.model.АвторыEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.sql.SQLException;
import java.util.List;

public class АвторыService extends SessionUtil implements АвторыDAO {
    @Override
    public void add(АвторыEntity author) throws SQLException {
        //открываем транзакцию
        openTransaction();
        Session session = getSession();
        session.save(author);
        closeTransaction();
    }

    @Override
    public List<АвторыEntity> getAll() throws SQLException {
        openTransaction();
        String sql = "select * from авторы";
        Session session = getSession();
        Query query = session.createNativeQuery(sql).addEntity(АвторыEntity.class);
        List<АвторыEntity> author = query.list();
        closeTransaction();
        return author;
    }

    @Override
    public АвторыEntity getById(int id) throws SQLException {
        openTransaction();
        String sql = "select * from авторы where id = 1";
        Session session = getSession();
        Query query = session.createNativeQuery(sql).addEntity(АвторыEntity.class);
        //query.setParameter("id", id);
        АвторыEntity author = (АвторыEntity) query.getSingleResult();
        closeTransaction();
        return  author;
    }

    @Override
    public void update(АвторыEntity author) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.update(author);
        closeTransaction();
    }

    @Override
    public void delete(АвторыEntity author) throws SQLException {
        openTransaction();
        Session session = getSession();
        session.remove(author);
        closeTransaction();
    }
}
