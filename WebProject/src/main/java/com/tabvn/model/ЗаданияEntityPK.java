package com.tabvn.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ЗаданияEntityPK implements Serializable {
    private int idЗадания;
    private int idРазделы;

    @Column(name = "idЗадания", nullable = false)
    @Id
    public int getIdЗадания() {
        return idЗадания;
    }

    public void setIdЗадания(int idЗадания) {
        this.idЗадания = idЗадания;
    }

    @Column(name = "idРазделы", nullable = false)
    @Id
    public int getIdРазделы() {
        return idРазделы;
    }

    public void setIdРазделы(int idРазделы) {
        this.idРазделы = idРазделы;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ЗаданияEntityPK that = (ЗаданияEntityPK) o;

        if (idЗадания != that.idЗадания) return false;
        if (idРазделы != that.idРазделы) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idЗадания;
        result = 31 * result + idРазделы;
        return result;
    }
}
