package com.tabvn.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class АвторыEntityPK implements Serializable {
    private int idАвторы;
    private int idРазделы;

    @Column(name = "idАвторы", nullable = false)
    @Id
    public int getIdАвторы() {
        return idАвторы;
    }

    public void setIdАвторы(int idАвторы) {
        this.idАвторы = idАвторы;
    }

    @Column(name = "idРазделы", nullable = false)
    @Id
    public int getIdРазделы() {
        return idРазделы;
    }

    public void setIdРазделы(int idРазделы) {
        this.idРазделы = idРазделы;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        АвторыEntityPK that = (АвторыEntityPK) o;

        if (idАвторы != that.idАвторы) return false;
        if (idРазделы != that.idРазделы) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idАвторы;
        result = 31 * result + idРазделы;
        return result;
    }
}
