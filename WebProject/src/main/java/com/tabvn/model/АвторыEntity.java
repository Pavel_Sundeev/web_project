package com.tabvn.model;

import javax.persistence.*;

@Entity
@Table(name = "авторы")
//@IdClass(АвторыEntityPK.class)
public class АвторыEntity {
    @Id
    @GeneratedValue
    @Column(name = "idАвторы", nullable = false)
    private int idАвторы;
    @OneToMany(mappedBy = "idАвторы", fetch = FetchType.LAZY)
    @JoinColumn(name = "idРазделы")
    private int idРазделы;
    @Column(name = "Фамилия", nullable = false)
    private String фамилия;
    @Column(name = "Имя", nullable = false)
    private String имя;
    @Column(name = "Название", nullable = false)
    private String название;

    public int getIdАвторы() {
        return idАвторы;
    }

    public void setIdАвторы(int idАвторы) {
        this.idАвторы = idАвторы;
    }

    public int getIdРазделы() {
        return idРазделы;
    }

    public void setIdРазделы(int idРазделы) {
        this.idРазделы = idРазделы;
    }

    //@Column(name = "Фамилия", nullable = false, length = 45)
    public String getФамилия() {
        return фамилия;
    }

    public void setФамилия(String фамилия) {
        this.фамилия = фамилия;
    }

        //@Column(name = "Имя", nullable = false, length = 45)
    public String getИмя() {
        return имя;
    }

    public void setИмя(String имя) {
        this.имя = имя;
    }

    //@Column(name = "Название", nullable = false, length = 255)
    public String getНазвание() {
        return название;
    }

    public void setНазвание(String название) {
        this.название = название;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        АвторыEntity that = (АвторыEntity) o;
//
//        if (idАвторы != that.idАвторы) return false;
//        if (idРазделы != that.idРазделы) return false;
//        if (фамилия != null ? !фамилия.equals(that.фамилия) : that.фамилия != null) return false;
//        if (имя != null ? !имя.equals(that.имя) : that.имя != null) return false;
//        if (название != null ? !название.equals(that.название) : that.название != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = idАвторы;
//        result = 31 * result + idРазделы;
//        result = 31 * result + (фамилия != null ? фамилия.hashCode() : 0);
//        result = 31 * result + (имя != null ? имя.hashCode() : 0);
//        result = 31 * result + (название != null ? название.hashCode() : 0);
//        return result;
//    }
}
