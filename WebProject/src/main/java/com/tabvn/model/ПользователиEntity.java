package com.tabvn.model;

import javax.persistence.*;

@Entity
@Table(name = "пользователи")
@IdClass(ПользователиEntityPK.class)
public class ПользователиEntity {
    @Id
    @GeneratedValue
    @Column(name = "idПользователи", nullable = false)
    private int idПользователи;
    @OneToMany(mappedBy = "пользователи", fetch = FetchType.LAZY)
    @JoinColumn(name = "idКатегории")
    private int idКатегории;
    @Column(name = "Имя", nullable = false, length = 45)
    private String имя;
    @Column(name = "Фамилия", nullable = false)
    private String фамилия;
    @Column(name = "Почта", nullable = false, length = 45)
    private String почта;
    @Column(name = "Пароль", nullable = false)
    private String пароль;

    public int getIdПользователи() {
        return idПользователи;
    }

    public void setIdПользователи(int idПользователи) {
        this.idПользователи = idПользователи;
    }

    public int getIdКатегории() {
        return idКатегории;
    }

    public void setIdКатегории(int idКатегории) {
        this.idКатегории = idКатегории;
    }

    //@Basic
    //@Column(name = "Имя", nullable = false, length = 45)
    public String getИмя() {
        return имя;
    }

    public void setИмя(String имя) {
        this.имя = имя;
    }

    //@Basic
    //@Column(name = "Фамилия", nullable = false, length = 45)
    public String getФамилия() {
        return фамилия;
    }

    public void setФамилия(String фамилия) {
        this.фамилия = фамилия;
    }

    //@Basic
    //@Column(name = "Почта", nullable = false, length = 45)
    public String getПочта() {
        return почта;
    }

    public void setПочта(String почта) {
        this.почта = почта;
    }

    //@Basic
    //@Column(name = "Пароль", nullable = false, length = 45)
    public String getПароль() {
        return пароль;
    }

    public void setПароль(String пароль) {
        this.пароль = пароль;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        ПользователиEntity that = (ПользователиEntity) o;
//
//        if (idПользователи != that.idПользователи) return false;
//        if (idКатегории != that.idКатегории) return false;
//        if (имя != null ? !имя.equals(that.имя) : that.имя != null) return false;
//        if (фамилия != null ? !фамилия.equals(that.фамилия) : that.фамилия != null) return false;
//        if (почта != null ? !почта.equals(that.почта) : that.почта != null) return false;
//        if (пароль != null ? !пароль.equals(that.пароль) : that.пароль != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = idПользователи;
//        result = 31 * result + idКатегории;
//        result = 31 * result + (имя != null ? имя.hashCode() : 0);
//        result = 31 * result + (фамилия != null ? фамилия.hashCode() : 0);
//        result = 31 * result + (почта != null ? почта.hashCode() : 0);
//        result = 31 * result + (пароль != null ? пароль.hashCode() : 0);
//        return result;
//    }
}
