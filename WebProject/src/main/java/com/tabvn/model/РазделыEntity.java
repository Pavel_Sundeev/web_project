package com.tabvn.model;

import javax.persistence.*;

@Entity
@Table(name = "разделы")
@IdClass(РазделыEntityPK.class)
public class РазделыEntity {
    @Id
    @GeneratedValue
    @Column(name = "idРазделы", nullable = false)
    private int idРазделы;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idКатегории")
    private int idКатегории;
    @Basic
    @Column(name = "Название", nullable = false, length = 45)
    private String название;
    @Basic
    @Column(name = "Количество", nullable = false)
    private int количество;

    public int getIdРазделы() {
        return idРазделы;
    }

    public void setIdРазделы(int idРазделы) {
        this.idРазделы = idРазделы;
    }

    public int getIdКатегории() {
        return idКатегории;
    }

    public void setIdКатегории(int idКатегории) {
        this.idКатегории = idКатегории;
    }

    public String getНазвание() {
        return название;
    }

    public void setНазвание(String название) {
        this.название = название;
    }


    public int getКоличество() {
        return количество;
    }

    public void setКоличество(int количество) {
        this.количество = количество;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        РазделыEntity that = (РазделыEntity) o;
//
//        if (idРазделы != that.idРазделы) return false;
//        if (idКатегории != that.idКатегории) return false;
//        if (количество != that.количество) return false;
//        if (название != null ? !название.equals(that.название) : that.название != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = idРазделы;
//        result = 31 * result + idКатегории;
//        result = 31 * result + (название != null ? название.hashCode() : 0);
//        result = 31 * result + количество;
//        return result;
//    }
}
