package com.tabvn.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class РазделыEntityPK implements Serializable {
    private int idРазделы;
    private int idКатегории;

    @Column(name = "idРазделы", nullable = false)
    @Id
    public int getIdРазделы() {
        return idРазделы;
    }

    public void setIdРазделы(int idРазделы) {
        this.idРазделы = idРазделы;
    }

    @Column(name = "idКатегории", nullable = false)
    @Id
    public int getIdКатегории() {
        return idКатегории;
    }

    public void setIdКатегории(int idКатегории) {
        this.idКатегории = idКатегории;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        РазделыEntityPK that = (РазделыEntityPK) o;

        if (idРазделы != that.idРазделы) return false;
        if (idКатегории != that.idКатегории) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idРазделы;
        result = 31 * result + idКатегории;
        return result;
    }
}
