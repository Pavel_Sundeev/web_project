package com.tabvn.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ПользователиEntityPK implements Serializable {
    private int idПользователи;
    private int idКатегории;

    @Column(name = "idПользователи", nullable = false)
    @Id
    public int getIdПользователи() {
        return idПользователи;
    }

    public void setIdПользователи(int idПользователи) {
        this.idПользователи = idПользователи;
    }

    @Column(name = "idКатегории", nullable = false)
    @Id
    public int getIdКатегории() {
        return idКатегории;
    }

    public void setIdКатегории(int idКатегории) {
        this.idКатегории = idКатегории;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ПользователиEntityPK that = (ПользователиEntityPK) o;

        if (idПользователи != that.idПользователи) return false;
        if (idКатегории != that.idКатегории) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idПользователи;
        result = 31 * result + idКатегории;
        return result;
    }
}
