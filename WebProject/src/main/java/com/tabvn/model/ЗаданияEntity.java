package com.tabvn.model;

import javax.persistence.*;

@Entity
@Table(name = "задания")
//@IdClass(ЗаданияEntityPK.class)
public class ЗаданияEntity {
    @Id
    @GeneratedValue
    @Column(name = "idЗадания", nullable = false)
    private int idЗадания;
    //@GeneratedValue // по поводу этой аннотации не совсем уверен
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idРазделы", nullable = false)
    private int idРазделы;
    @Column(name = "Номер", nullable = false)
    private int номер;
    @Column(name = "Название", nullable = false)
    private String название;

    public int getIdЗадания() {
        return idЗадания;
    }

    public void setIdЗадания(int idЗадания) {
        this.idЗадания = idЗадания;
    }

    public int getIdРазделы() {
        return idРазделы;
    }

    public void setIdРазделы(int idРазделы) {
        this.idРазделы = idРазделы;
    }

    public int getНомер() {
        return номер;
    }

    public void setНомер(int номер) {
        this.номер = номер;
    }

    //@Basic
    //@Column(name = "Название", nullable = false, length = 45)
    public String getНазвание() {
        return название;
    }

    public void setНазвание(String название) {
        this.название = название;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        ЗаданияEntity that = (ЗаданияEntity) o;
//
//        if (idЗадания != that.idЗадания) return false;
//        if (idРазделы != that.idРазделы) return false;
//        if (номер != that.номер) return false;
//        if (название != null ? !название.equals(that.название) : that.название != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = idЗадания;
//        result = 31 * result + idРазделы;
//        result = 31 * result + номер;
//        result = 31 * result + (название != null ? название.hashCode() : 0);
//        return result;
//    }
}
