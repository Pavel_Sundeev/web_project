package com.tabvn.model;

import javax.persistence.*;

@Entity
@Table(name = "категории")
public class КатегорииEntity {
    @Id
    @GeneratedValue
    @Column(name = "idКатегории", nullable = false)
    private int idКатегории;
    @Column(name = "Наименование_предмета", nullable = false)
    private String наименованиеПредмета;

    public int getIdКатегории() {
        return idКатегории;
    }

    public void setIdКатегории(int idКатегории) {
        this.idКатегории = idКатегории;
    }

    //@Basic
    //@Column(name = "Наименование_предмета", nullable = false, length = 45)
    public String getНаименованиеПредмета() {
        return наименованиеПредмета;
    }

    public void setНаименованиеПредмета(String наименованиеПредмета) {
        this.наименованиеПредмета = наименованиеПредмета;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        КатегорииEntity that = (КатегорииEntity) o;
//
//        if (idКатегории != that.idКатегории) return false;
//        if (наименованиеПредмета != null ? !наименованиеПредмета.equals(that.наименованиеПредмета) : that.наименованиеПредмета != null)
//            return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = idКатегории;
//        result = 31 * result + (наименованиеПредмета != null ? наименованиеПредмета.hashCode() : 0);
//        return result;
//    }
}
