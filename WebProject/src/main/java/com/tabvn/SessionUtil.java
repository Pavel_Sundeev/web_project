package com.tabvn;

import com.tabvn.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class SessionUtil {

    private Session session;
    private Transaction transaction;

    public Session getSession(){
        return session;
    }

    public Transaction getTransaction(){
        return transaction;
    }

    //не совсем уверен что правильно реализовал
    public Session openSession(){
        return HibernateUtil.getFactory().openSession();
    }

    //таке же не уверен что правильно все сделал, еадо будет перепроверить
    public Session openTransaction(){
        session = openSession();
        transaction = session.beginTransaction();
        return session;
    }

    public void closeSession(){
        session.close();
    }

    public  void closeTransaction(){
        transaction.commit();
        closeSession();
    }
}
