package com.tabvn.servlet;

import com.tabvn.HibernateUtil;
import com.tabvn.model.АвторыEntity;
import com.tabvn.model.КатегорииEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/users")
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/templates/users.jsp").forward(request, response);

        КатегорииEntity category = new КатегорииEntity();
//        category.setIdКатегории(1);
        category.setНаименованиеПредмета("ewefwe");

        Session session = HibernateUtil.getFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(category);
        transaction.commit();
        session.close();
        request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
    }
}
