package com.tabvn.servlet;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;

@Named
public class CheckPasswordAndLogin {
    private String login;
    private String password;
    private boolean bool;
    private String textL = "Введите логин";
    private String textP = "Введите пароль";

    public String getTextL(){
        return textL;
    }

    public String getTextP(){
        return textP;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isBoolean(){
        return bool;
    }
}
